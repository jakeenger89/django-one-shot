from django.urls import path
from todos.views import todo_list_list, todo_list_detail
from todos.views import todo_list_create, todo_list_update
from todos.views import todo_list_delete, todo_item_create

urlpatterns = [
    path("", todo_list_list, name="todo_list_list"),
    path("<int:id>/", todo_list_detail, name="todo_list_detail"),
    path("create/", todo_list_create, name="todo_list_create"),
    path("<int:id>/edit/", todo_list_update, name="todo_list_update"),
    path("<int:id>/delete/", todo_list_delete, name="todo_list_delete"),
    path("items/create/", todo_item_create, name="todo_item_create"),
]


# the path "int edit" shows on your hot bar when searching online
# thats how you define what the user searches

# the name = string is how we can refer to it within
# the html ie with a url link
